package com.br.calculadora.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class OperacoesTest {

    @Test
    public void testarOperacaoSoma() {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(6, Operacao.soma(numeros));
    }

    @Test
    public void testarOperacaoSomaDeDoisNumeros() {
        Assertions.assertEquals(2, Operacao.soma(1, 1));
    }

    @Test
    public void testarValorInvalidoNaDivisao() {
        Assertions.assertThrows(ArithmeticException.class, () -> {
            Operacao.divisao(0, 1);
        });
    }

    @Test
    public void testarOperacaoDivisaoDeDoisNumeros() {
        Assertions.assertEquals(3, Operacao.divisao(6, 2));
    }

    @Test
    public void testarValorInvalidoNaDivisaoEmLista() {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(0);
        numeros.add(1);
        Assertions.assertThrows(ArithmeticException.class, () -> {
            Operacao.divisao(numeros);
        });
    }

    @Test
    public void testarOperacaoDivisaoEmLista() {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(6);
        numeros.add(2);
        Assertions.assertEquals(3, Operacao.divisao(numeros));
    }

    @Test
    public void testarOperacaoMultiplicacaoEmLista() {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(2);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(12, Operacao.multiplicacao(numeros));
    }

    @Test
    public void testarOperacaoMultiplicacaoComDoisNumeros() {
        Assertions.assertEquals(4, Operacao.multiplicacao(2, 2));
    }

    @Test
    public void testarOperacaoSubtracaoEmLista() {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(2);
        numeros.add(2);
        Assertions.assertEquals(0, Operacao.subtracao(numeros));
    }

    @Test
    public void testarOperacaoSubtracaoComDoisNumeros() {
        Assertions.assertEquals(0, Operacao.subtracao(2, 2));
    }

}
