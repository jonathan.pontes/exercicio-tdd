package com.br.calculadora.demo;

import java.util.Collections;
import java.util.List;

public class Operacao {

    public static int soma(int numUm, int numDois) {
        int resultado = numUm + numDois;
        return resultado;
    }

    public static int soma(List<Integer> numeros) {
        int resultado = 0;
        for (Integer numero : numeros) {
            resultado += numero;
        }
        return resultado;
    }

    public static int multiplicacao(List<Integer> numeros) {
        int resultado = 1;
        for (int numero : numeros) {
            resultado *= numero;
        }
        return resultado;
    }

    public static int multiplicacao(int numUm,int numDois) {
        int resultado = numUm * numDois;
        return resultado;
    }

    public static int divisao(int numUm, int numDois) {
        if (numUm != 0) {
            int resultado = (numUm / numDois);
            return resultado;
        } else {
            throw new ArithmeticException("O valor zero para o dividendo é invalido!");
        }
    }

    public static int divisao(List<Integer> numeros) {
        if (numeros.get(0).intValue() != 0) {
            return (numeros.get(0).intValue())/(numeros.get(1).intValue());
        }else{
            throw new ArithmeticException("O valor zero para o dividendo é invalido!");
        }

    }

    public static int subtracao(List<Integer> numeros) {
        Integer primeiroElemento = numeros.get(0);
        numeros.remove(0);
        int resultado = primeiroElemento;
        for (int numero : numeros) {
            resultado -= numero;
        }
        return resultado;
    }

    public static int subtracao(int numUm, int numDois) {
        int resultado = numUm - numDois;
        return resultado;
    }

}
